source 'https://rubygems.org'

# Load env from file
gem 'dotenv-rails'

# CSS Framework
gem 'semantic-ui-sass', github: 'doabit/semantic-ui-sass'

# Silence asset logging
gem 'quiet_assets', group: :development

# Loading indicator
gem 'nprogress-rails'

# User sessions
gem 'omniauth-steam'

# Api wrapper
gem 'dota'
# Forked version for when dota updates
#gem 'dota', :git => 'https://github.com/efy/dota.git', :branch => 'update-686'

# Background worker queue
gem 'resque' # v1.x
gem 'resque-retry'

# Mostly for scheduling match updates
gem 'rufus-scheduler', '~> 3.1.4'

# Client lib for sending application metrics
gem 'statsd-instrument', '~> 2.1'

# ORM for rethinkdb
gem 'nobrainer'

# Pageination
gem 'kaminari'
gem 'kaminari-nobrainer'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.3'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.1.0'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

# Util helper for active links
gem 'active_link_to'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# foreman to create service in production
gem 'foreman', '~> 0.78.0'

group :production do
  # Use Unicorn as the app server
  gem 'unicorn'
  gem 'rails_12factor', group: :production
end

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Test framework
  gem 'rspec-rails', '~> 3.0'
  gem 'factory_girl_rails'

  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  # JSON parser for testing example payloads
  gem 'json'
end

group :development do
  gem 'rack-mini-profiler'
end
