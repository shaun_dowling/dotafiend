Rails.application.routes.draw do

  # Account routes
  get '/user/:uid/account', to: 'account#show', as: 'account'
  put '/user/:uid/account', to: 'account#update', as: 'account_update'

  get '/request-match-history', to: 'account#request_match_history', as: 'request_match_history'

  root 'home#index'

  # Session routes
  match '/auth/:provider/callback', to: 'sessions#create', via: :all
  delete '/logout', to: 'sessions#destroy', as: :logout


  # Player Dashboard
  get '/user/:uid/dashboard', to: 'dashboard#index', as: 'dashboard'

  # Player comparisons
  scope '/user/:uid' do
    resources :comparisons
  end

  # Player friends
  get '/user/:uid/friends', to: 'friends#index', as: 'friends'

  # Player matches
  get '/user/:uid/matches', to: 'matches#index', as: 'matches'

  # Player matches
  get '/user/:uid/heroes', to: 'heroes#index', as: 'heroes'

  # mount resque web interface
  mount Resque::Server.new, :at => "/resque"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
