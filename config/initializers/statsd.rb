
StatsD.prefix = "dotafiend"
StatsD.default_sample_rate = 1

STATSD_TAGS = ["env:#{Rails.env}"]

REQUEST_METRICS = {
  "request.success"               => 200,
  "request.redirect"              => 302,
  "request.bad_request"           => 400,
  "request.not_found"             => 404,
  "request.too_many_requests"     => 429,
  "request.internal_server_error" => 500,
  "request.bad_gateway"           => 502
}


RequestMonitor.extend(StatsD::Instrument)
RequestMonitor.statsd_measure(:call, "request.duration", tags: STATSD_TAGS)

REQUEST_METRICS.each do |name, code|
  RequestMonitor.statsd_count_if(:call, name, tags: STATSD_TAGS) do |status, _env, _body|
    status.to_i == code
  end
end

SessionsController.extend(StatsD::Instrument)
SessionsController.statsd_count_if(:create, "logins", tags: STATSD_TAGS)
