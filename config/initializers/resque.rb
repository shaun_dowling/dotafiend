require 'resque'
require 'resque-retry'
require 'resque/failure/redis'

# Resque web interface
require 'resque/server'
require 'resque-retry/server'

# Set failure backend for matches
Resque::Failure::MultipleWithRetrySuppression.classes = [Resque::Failure::Redis]
Resque::Failure.backend = Resque::Failure::MultipleWithRetrySuppression

# Protect resque web interface
Resque::Server.use(Rack::Auth::Basic) do |user, password|
  password == ENV["RESQUE_PASSWORD"] && user == ENV["RESQUE_USER"]
end
