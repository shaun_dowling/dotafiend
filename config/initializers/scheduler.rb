require 'rufus-scheduler'

s = Rufus::Scheduler.singleton

s.every '30s' do
  user = User.sample
  if !user.nil?
    Resque.enqueue(LatestMatchesJob, user.uid)
  end
end
