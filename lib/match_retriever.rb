
class MatchesRetriever
  def get_match(id)
    match = Dota.api.get("IDOTA2Match_570", "GetMatchDetails", match_id: id)
    if match["result"]["error"].nil? 
      match
    else
      raise MatchRetrievalError.new "Error Retrieving Match: #{id}"
    end
  end

  def get_and_store_match(id)
    begin
      match_hash = get_match(id)
      result = match_hash["result"]
      match = Match.new(
        uid: result["match_id"], 
        duration: result["duration"],
        start_time: Time.at(result["start_time"]),
        radiant_win: result["radiant_win"],
        result: match_hash
      )
      if match.save
        return true
      else
        return false
      end
    rescue MatchRetrievalError
      return false
    end
  end

  class MatchRetrievalError < StandardError
  end
end

