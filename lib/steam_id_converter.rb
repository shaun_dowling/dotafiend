module SteamIdConverter
  MASTER = 76561197960265728

  def SteamIdConverter.to_64(steam_32)
    steam_32 + MASTER
  end

  def SteamIdConverter.to_32(steam_64)
    steam_64 - MASTER
  end
end
