
class UserMatchesRetriever
  attr_reader :player_id

  def initialize(player_id) 
    @player_id = player_id
  end

  def get_history(starting_id = nil)
    if !starting_id 
      Dota.api.matches(player_id: @player_id)
    else
      Dota.api.matches(player_id: @player_id, after: starting_id)
    end
  end
end
