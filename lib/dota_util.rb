
module DotaUtil
  LEAVER_STATUSES = YAML.load(File.read(File.expand_path('../leaver_status.yaml', __FILE__)))
  HEROES = YAML.load(File.read(File.expand_path('../heroes.yaml', __FILE__)))
  IMAGE_ENDPOINT = "http://cdn.dota2.com/apps/dota2/images/heroes/{name}_sb.png"


  def DotaUtil.hero_name(id)
    HEROES[id]["localized_name"]
  end

  def DotaUtil.hero_image(id)
    name = HEROES[id]["name"].gsub("npc_dota_hero_", "")
    IMAGE_ENDPOINT.gsub("{name}", name)
  end

  def DotaUtil.leaver_status(id)
    LEAVER_STATUSES[id]
  end

  # Wraps the Dota.api method and
  # returns a Dota::API::Item instance
  def DotaUtil.item(id)
    Dota.api.items.select {|i| i.id == id}.first
  end
end
