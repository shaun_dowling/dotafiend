require 'rails_helper'
require 'steam_id_converter'

RSpec.describe SteamIdConverter do
  describe "#to_32" do
    it "converts a 64 bit id to a 32 bit id" do
      steam_64 = 76561197989436298
      expected = 29170570 
      converted = SteamIdConverter.to_32(steam_64)

      expect(converted).to eq(expected)
    end
  end

  describe "#to_64" do
    it "converts a 32 bit id to a 64 bit id" do
      steam_32 = 29170570 
      expected = 76561197989436298
      converted = SteamIdConverter.to_64(steam_32)

      expect(converted).to eq(expected)
    end
  end
end

