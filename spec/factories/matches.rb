FactoryGirl.define do
  factory :match do
    uid 1
    start_time Time.now
    duration 4000
    radiant_win true 
    result Hash.new
  end
end
