require 'rails_helper'

RSpec.describe SessionsController, type: :controller do

  describe "POST #create" do
    it "redirects to the users dashboard if auth succeeds" do
      request.env['omniauth.auth'] = OmniAuth.config.mock_auth[:steam]
      post :create, provider: :steam
      
      expect(response).to redirect_to(dashboard_path)
    end
  end

  describe "DELETE #destroy" do
    it "redirects to the homepage" do
      delete :destroy
      expect(response).to redirect_to(root_path)
    end
  end

end
