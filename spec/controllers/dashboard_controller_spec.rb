require 'rails_helper'
require 'spec_helper'

RSpec.describe DashboardController, type: :controller do

  describe "GET #index" do

    describe "authenticated" do

      before(:each) do
        # TODO Refactor into sign_in helper method
        user = User.from_omniauth(OmniAuth.config.mock_auth[:steam])
        session[:user_id] = user.id
      end

      it "returns http success" do
        get :index
        expect(response).to have_http_status(:success)
      end

      it "renders the index template" do
        get :index
        expect(response).to render_template(:index)
      end
    end

    describe "not authenticated" do
      it "should redirect to the homepage" do
        get :index
        expect(response).to redirect_to(root_path)
      end
    end
  end

end
