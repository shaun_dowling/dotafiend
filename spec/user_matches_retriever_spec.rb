require 'rails_helper'
require 'user_matches_retriever'
require 'dota'

RSpec.describe UserMatchesRetriever do
  before(:each) do
    @retriever = UserMatchesRetriever.new(29170570)
  end

  describe "#new" do
    it "instantiates a new instance" do
      expect(@retriever.player_id).to eq(29170570)
    end
  end

  describe "#get_history" do
    before do
      @history = @retriever.get_history
    end

    it "returns an array" do
      expect(@history.class).to be(Array)
    end

    it "returns an array of match objects" do
      expect(@history.first.class).to eq(Dota::API::Match)
    end

    it "returns an array with a max length of 100" do
      expect(@history.length).to be <= 100
    end

    it "given a starting match will return matches after and including it" do
      first_matches = @history 
      second_matches = @retriever.get_history(first_matches.last.id)

      expect(first_matches.last.id).to eq(second_matches.first.id)
    end
  end
end
