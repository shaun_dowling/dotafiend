
require 'rails_helper'
require 'dota_util'

RSpec.describe DotaUtil do
  it "is a module" do
    expect(DotaUtil.class).to eq(Module)
  end

  describe "HEROES" do
    it "is a hash" do
      expect(DotaUtil::HEROES.class).to eq(Hash)
    end

    it "loads the hero lookup table" do
      expect(DotaUtil::HEROES[1]["localized_name"]).to eq('Anti-Mage')
    end
  end

  describe "#hero_name" do
    it "returns the heroes localized name" do
      expect(DotaUtil.hero_name(1)).to eq('Anti-Mage')
    end
  end

  describe "#hero_image" do
    it "returns a hero portrait url" do
      expected_url = "http://cdn.dota2.com/apps/dota2/images/heroes/antimage_sb.png"
      expect(DotaUtil.hero_image(1)).to eq(expected_url)
    end
  end
end

