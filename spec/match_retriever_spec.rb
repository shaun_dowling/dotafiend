require 'rails_helper'
require 'match_retriever'
require 'dota'

RSpec.describe MatchesRetriever do
  before do
    @r = MatchesRetriever.new
    @valid_match_id = 1794259426
  end

  describe "#new" do
    it "creates an instance" do
      @r = MatchesRetriever.new
      expect(@r.class).to eq(MatchesRetriever)
    end
  end
  
  describe "#get_match" do

    it "expects an ID parameter" do
      expect{@r.get_match}.to raise_error(ArgumentError)
    end

    it "raises an error if the match result container an error" do
      expect{@r.get_match(123)}.to raise_error(MatchesRetriever::MatchRetrievalError)
    end

    it "returns a hash when the match is valid" do
      expect(@r.get_match(@valid_match_id).class).to eq(Hash)  
    end
  end

  describe "#get_and_store_match" do
    before(:each) do
      NoBrainer.purge!
    end

    it "accepts one argument" do
      expect{@r.get_and_store_match(123)}.not_to raise_error
    end

    it "returns true when the match is saved" do
      expect(@r.get_and_store_match(@valid_match_id)).to eq(true)
    end

    it "returns false when the match is invalid" do
      expect(@r.get_and_store_match(123)).to eq(false)
    end

    it "stores a new match object in the database" do
      @r.get_and_store_match(@valid_match_id)
      match = Match.where(uid: @valid_match_id).first

      expect(match.class).to eq(Match)
    end
  end

end

