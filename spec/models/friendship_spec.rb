require 'rails_helper'

RSpec.describe Friendship, type: :model do
  it "requires a friend_uid" do
    expect(Friendship.new.valid?).to be(false)
  end

  it "requires a user_uid" do
    expect(Friendship.new(friend_uid: 1).valid?).to be(false)
  end

  it "is unique on user_uid and friend_uid" do
    f1 = Friendship.new(friend_uid: 1, user_uid: 2)
    f1.save

    f2 = Friendship.new(friend_uid:1, user_uid: 2)
    expect(f2.valid?).to be(false)
  end

  describe "additional attributes" do
    before(:all) do
      @friendship = Friendship.new(friend_uid: 1, user_uid: 2)
    end

    it "relationship" do
      expect{@friendship.relationship}.to_not raise_error
    end

    it "friended_at" do
      expect{@friendship.friended_at}.to_not raise_error
    end
  end
end
