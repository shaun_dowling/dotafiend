require 'rails_helper'
require 'omniauth'


RSpec.describe User, type: :model do
  before(:all) do
    NoBrainer.sync_schema
  end

  before(:each) do
    NoBrainer.purge!
  end

  describe "attributes" do

    before(:all) do
      @user = User.new(uid: 123)
    end

    it "has uid attribute" do
      expect(@user.uid).to eq(123)
    end

    it "uid is an integer" do
      expect(@user.uid.class).to eq(Fixnum)
    end

    it "has a nickname attribute" do
      expect{@user.nickname}.to_not raise_error
    end

    it "has a image attribute" do
      expect{@user.image}.to_not raise_error
    end

    it "has a name attribute" do
      expect{@user.name}.to_not raise_error
    end

    it "has a profile_url attribute" do
      expect{@user.profile_url}.to_not raise_error
    end

    it "is invalid with an existing uid" do
      @user.save
      user_two = User.new(uid: 123)

      expect(user_two.valid?).to be(false)
    end
  end

  describe "#save" do
    it "persists the user model to the database" do
      user = User.new(uid: 999, nickname: 'Test')

      user.save
      retrieved_user = User.where({uid: 999}).first

      expect(retrieved_user.uid).to eq(999)
      expect(retrieved_user.nickname).to eq("Test")
    end
  end

  describe "User.from_omniauth" do
    before(:all) do
      mock_auth_hash = OmniAuth::AuthHash.new({
        uid: 76561197989436298,
        provider: 'steam',
        info: {
          image: 'http://test.com/test.png',
          nickname: 'Test Nickname',
          name: 'Test'
        }
      })

      @user = User.from_omniauth(mock_auth_hash)
    end

    it "creates a user from an OmniAuth hash" do
      expect(@user.uid).to eq(29170570)
    end

    it "sets info attributes" do
      expect(@user.name).to eq('Test')
      expect(@user.nickname).to eq('Test Nickname')
      expect(@user.image).to eq('http://test.com/test.png')
    end
  end

  describe "#match_summaries" do
    it "returns match summaries for player" do
      user = User.create(uid: 1)
      match_summary = UserMatchSummary.create(user_uid: 1, match_uid: 1) 

      expect(user.match_summaries.length).to eq(1)
    end
  end

  describe "Friendships" do
    it "user has many friendships" do
      user = User.create(uid: 1)
      Friendship.create(user_uid: 1, friend_uid: 2)
      Friendship.create(user_uid: 1, friend_uid: 3)

      expect(user.friendships.count).to eq(2)
    end
  end

  describe "#friends" do
    it "returns the users friends" do
      user1 = User.create(uid: 1)
      user2 = User.create(uid: 2)

      Friendship.create(user_uid: 1, friend_uid: 2)

      expect(user1.friends.count).to eq(1)
    end
  end
end
