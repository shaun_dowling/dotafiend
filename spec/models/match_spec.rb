require 'rails_helper'

RSpec.describe Match, type: :model do
  before(:all) do
    NoBrainer.sync_schema
  end

  before(:each) do
    NoBrainer.purge!
  end

  it "is invalid without a uid" do
    expect(Match.new.valid?).to be(false)
  end

  it "is invalid without a start time" do
    match = Match.new(uid: 111)
    
    expect(match.valid?).to be(false)
  end

  it "is invalid without a duration" do
    match = Match.new(uid: 222, start_time: Time.now)

    expect(match.valid?).to be(false)
  end

  it "is invalid without a radiant_win" do
    match = Match.new(uid: 333, start_time: Time.now, duration: 3333)
    
    expect(match.valid?).to be(false)
  end

  it "is invalid with a duplicate match_id" do
    match_one = Match.new(uid: 123)
    match_two = Match.new(uid: 123)

    match_one.save

    expect(match_two.valid?).to be(false)
  end

  describe "#save" do
    it "persists a valid record to the database" do
      match = Match.new(uid: 444, start_time: Time.now, duration: 4444, radiant_win: true)
      expect(match.save).to be(true)
    end
  end
end
