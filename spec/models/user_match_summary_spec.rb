require 'json'
require 'rails_helper'

RSpec.describe UserMatchSummary, type: :model do
  before(:all) do
    NoBrainer.sync_schema
  end

  before(:each) do
    NoBrainer.purge!
  end

  it "requires a match_uid field" do
    expect(UserMatchSummary.new.valid?).to eq(false)
  end

  it "requires a user_uid field" do
    ums = UserMatchSummary.new(match_uid: 123)
    expect(ums.valid?).to eq(false)
  end

  it "user_uid and match_uid should be of type Integer" do
    ums = UserMatchSummary.new(match_uid: 123, user_uid: 123)
    expect(ums.valid?).to be(true)
  end

  it "is unique on the match_uid and user_uid fields" do
    ums1 = UserMatchSummary.new(match_uid: 1, user_uid: 1)
    ums2 = UserMatchSummary.new(match_uid: 1, user_uid: 1)

    ums1.save

    expect(ums2.valid?).to be(false)
  end

  describe "#save" do
    it "persists the record to the database" do
      ums = build(:user_match_summary)

      expect(ums.save).to eq(true)
    end
  end

  describe "#build_from_match_hash" do
    file = File.expand_path("../../support/match.json", __FILE__)
    example_match = JSON.parse(File.read(file))

    ums = UserMatchSummary.new(user_uid: 22823152, match_uid: 1777955128)
    ums.build_from_match_hash(example_match)

    it "sets the match fields" do
      expect(ums.duration).to eq(2260)
      expect(ums.radiant_win).to eq(false)
      expect(ums.start_time.class).to eq(Time)
    end

    it "sets the player fields" do
      expect(ums.deaths).to eq(12)
      expect(ums.kills).to eq(5)
    end

    it "calculates and sets the player team" do
      expect(ums.team).to eq('Radiant')
    end

    it "calculates and sets the win field" do
      expect(ums.win).to eq(false)
    end
  end
end

