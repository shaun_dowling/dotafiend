# Skeleton middleware to allow statsd to measure response times and status codes

class RequestMonitor
  def initialize(app)
    @app = app
  end

  def call(env)
    @app.call(env)
  end
end
