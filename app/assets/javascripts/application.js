// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require nprogress
//= require nprogress-turbolinks
//= require semantic-ui
//= require_tree .

NProgress.configure({
  showSpinner: false
})

function ready() {
  // Hide broken images
  $('img').on('error', function(){
    $(this).css('visibility', 'hidden')
  })

  // Initialize Semantic UI modules
  $('.ui.dropdown').dropdown()
  $('.ui.checkbox').checkbox()
  $('.message .close').on('click', function() {
    $(this).closest('.message').transition('fade')
  })
  $('.dota-item-image, .dota-hero-image').popup({
    position: 'top center'
  })

  // Handle request match history action
  $('#request_match_history').on('click', function(e) {
    e.preventDefault()
    var button = $(this)
    var uri = button.data('uri')
    button.addClass('loading')
    $.get(uri).done(function(res){
      button.removeClass('loading')
    }).fail(function(){
      button.removeClass('loading')
    })
  })
}

// Handle regular ready
$(document).ready(ready)

// Handle turbolinks ready
$(document).on('page:load', ready)

