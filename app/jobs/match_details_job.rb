
class GetMatchDetailsJob < BaseApiJob
  def self.perform(match_id, user_id)
    retriever = MatchesRetriever.new
    retriever.get_and_store_match(match_id)

    # fetch the match and build match summary for player
    match = Match.where(uid: match_id).first
    ums = UserMatchSummary.new(user_uid: user_id, match_uid: match.uid)
    ums.build_from_match_hash(match.result)
    ums.save
  end
end

