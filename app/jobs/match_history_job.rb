
class MatchHistoryJob < BaseApiJob

  def self.perform(player_id, starting_match=nil)
    # Get the users lastest match
    retriever = UserMatchesRetriever.new(player_id)

    # fetch the users latest matches
    matches = retriever.get_history(starting_match)

    # Add job to queue for each match unless it exists
    matches.each do |match|
      persisted_match = Match.where(uid: match.id).first
      if persisted_match
        # if the match already exists create match summary
        ums = UserMatchSummary.new(user_uid: player_id, match_uid: persisted_match.uid)
        ums.build_from_match_hash(persisted_match.result)
        ums.save
      else
        Resque.enqueue(GetMatchDetailsJob, match.id, player_id)
      end
    end

    # Requeue starting from the last match
    Resque.enqueue(self, player_id, matches.last.id) unless matches.length < 100
  end
end

