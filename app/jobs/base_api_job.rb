
class BaseApiJob
  extend Resque::Plugins::Retry
  extend Resque::Plugins::ExponentialBackoff

  def self.queue
    :matches 
  end

  # Rate limit the perform method
  def self.before_perform(*args)
    @start_time = Time.now
  end

  def self.after_perform(*args)
    end_time = Time.now
    perform_time = (end_time - @start_time) * 1000
    if perform_time < 1000
      sleep (1000 - perform_time) / 1000
    end
  end

  # Configure retry timing
  def self.backoff_strategy
    [30, 60, 600, 3600, 10800, 21600]
  end
end
