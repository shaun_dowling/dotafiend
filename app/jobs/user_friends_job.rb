
class UserFriendsJob < BaseApiJob

  def self.perform(user_uid)
    api = Dota.api
    friends = api.friends(SteamIdConverter.to_64(user_uid))

    friends.each do |friend|
      friend_uid = SteamIdConverter.to_32(friend.id)

      friendship = Friendship.where(user_uid: user_uid, friend_uid: friend_uid).first_or_create
      friendship.user_uid = user_uid 
      friendship.friend_uid = friend_uid
      friendship.relationship = friend.relationship
      friendship.friended_at = Time.at(friend.friended_at)

      friendship.save
    end
  end

end
