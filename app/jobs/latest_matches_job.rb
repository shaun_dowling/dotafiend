
class LatestMatchesJob < BaseApiJob

  def self.perform(player_id)
    retriever = UserMatchesRetriever.new(player_id)
    matches = retriever.get_history

    if !matches
      return
    end

    matches.each do |match|
      persisted_match = Match.where(uid: match.id).first
      if persisted_match
        # if the match already exists create match summary
        ums = UserMatchSummary.new(user_uid: player_id, match_uid: persisted_match.uid)
        ums.build_from_match_hash(persisted_match.result)
        ums.save
      else
        Resque.enqueue(GetMatchDetailsJob, match.id, player_id)
      end
    end
  end

end
