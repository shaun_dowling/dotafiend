
class HeroMatchesJob < BaseApiJob

  def self.perform(player_id, hero_id, starting_match=nil)
    if starting_match.nil?
      matches = Dota.api.matches(player_id: player_id, hero_id: hero_id)
    else
      matches = Dota.api.matches(player_id: player_id, hero_id: hero_id, after: starting_match)
    end

    if !matches
      return
    end

    matches.each do |match|
      persisted_match = Match.where(uid: match.id).first
      if persisted_match
        # if the match already exists create match summary
        ums = UserMatchSummary.new(user_uid: player_id, match_uid: persisted_match.uid)
        ums.build_from_match_hash(persisted_match.result)
        ums.save
      else
        Resque.enqueue(GetMatchDetailsJob, match.id, player_id)
      end
    end

    # Requeue starting from the last match
    Resque.enqueue(self, player_id, hero_id, matches.last.id) unless matches.length < 100
  end

end
