class Match
  include NoBrainer::Document
  include NoBrainer::Document::Timestamps

  has_many :player_summaries, class_name: 'UserMatchSummary', primary_key: :uid, foreign_key: :match_uid

  field :uid, type: Integer, uniq: true, required: true
  field :start_time, type: Time, required: true
  field :duration, type: Integer, required: true
  field :radiant_win, type: Boolean, required: true
  field :result
end
