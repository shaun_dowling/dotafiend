class Friendship
  include NoBrainer::Document
  include NoBrainer::Document::Timestamps

  field :user_uid, :type => Integer, required: true, validates: {
    uniqueness: { scope: :friend_uid }
  }

  field :friend_uid, :type => Integer, required: true, validates: {
    uniqueness: { scope: :user_uid }
  }

  field :relationship, type: String
  field :friended_at, type: Time
end
