
class UserMatchSummary
  include NoBrainer::Document
  include NoBrainer::Document::Timestamps

  belongs_to :user, foreign_key: :uid, primary_key: :user_uid, index: true

  field :match_uid, type: Integer, required: true, validates: {
    uniqueness: {scope: :user_uid }
  }

  field :user_uid, type: Integer, required: true, index: true, validates: {
    uniqueness: { scope: :match_uid }
  }

  # Match stats
  field :start_time, type: Time
  field :duration, type: Integer
  field :radiant_win, type: Boolean
  field :win, type: Boolean

  # Player stats
  field :team, type: String
  field :win, type: Boolean

  field :deaths, type: Integer
  field :kills, type: Integer
  field :last_hits, type: Integer
  field :assists, type: Integer
  field :denies, type: Integer
  field :gold, type: Integer
  field :gold_per_min, type: Integer
  field :gold_spent, type: Integer
  field :hero_damage, type: Integer
  field :hero_healing, type: Integer
  field :hero_id, type: Integer
  field :items, type: Array
  field :leaver_status, type: Integer
  field :level, type: Integer
  field :player_slot, type: Integer
  field :tower_damage, type: Integer
  field :xp_per_min, type: Integer

  def build_from_match_hash(match_hash)
    result = match_hash["result"]

    # Set match stats
    self.duration = result["duration"]
    self.start_time = Time.at result["start_time"]
    self.radiant_win = result["radiant_win"]

    # Find the player
    player = result["players"].select do |p|
      p["account_id"] == self.user_uid
    end.first

    self.deaths = player["deaths"]
    self.kills = player["kills"]
    self.assists = player["assists"]
    self.denies = player["denies"]
    self.last_hits = player["last_hits"]
    self.gold = player["gold"]
    self.gold_per_min = player["gold_per_min"]
    self.gold_spent = player["gold_spent"]
    self.hero_damage = player["hero_damage"]
    self.hero_healing = player["hero_healing"]
    self.hero_id = player["hero_id"]
    self.leaver_status = player["leaver_status"]
    self.level = player["level"]
    self.player_slot = player["player_slot"]
    self.tower_damage = player["tower_damage"]
    self.xp_per_min = player["xp_per_min"]

    # Transform player item_# into array
    self.items = (0..5).map {|n| player["item_#{n}"]}

    # Calculate the player team
    if self.player_slot < 5
      self.team = "Radiant"
    else
      self.team = "Dire"
    end

    # Calculate if the player won
    if self.radiant_win && self.team == "Radiant"
      self.win = true
    elsif !self.radiant_win && self.team == "Dire"
      self.win = true
    else
      self.win = false
    end
  end

  def recalculate_attributes
    match = Match.where(uid: self.match_uid).first
    build_from_match_hash(match.result)
    save
  end
end
