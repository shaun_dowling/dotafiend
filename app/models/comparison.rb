class Comparison
  include NoBrainer::Document
  include NoBrainer::Document::Timestamps

  belongs_to :user

  field :user_uid, type: Integer
  field :title, type: String
  field :players, type: Array
  field :heroes, type: Array, default: []
end
