
class User

  include NoBrainer::Document
  include NoBrainer::Document::Timestamps

  has_many :match_summaries, class_name: 'UserMatchSummary', primary_key: :uid, foreign_key: :user_uid
  has_many :friendships, primary_key: :uid, foreign_key: :user_uid
  has_many :comparisons, primary_key: :uid, foreign_key: :user_uid

  field :uid, type: Integer, uniq: true, index: true
  field :nickname, type: String
  field :image, type: String
  field :name, type: String
  field :email, type: String
  field :weekly_reports_preference, type: Boolean, default: true
  field :feature_updates_preference, type: Boolean, default: true

  def friends
    friends_array = friendships.map do |friendship|
      User.where(uid: friendship.friend_uid).first
    end

    friends_array.select {|f| !f.nil? }
  end

  def wins
    self.match_summaries.select {|m| m.win == true }.count
  end

  def losses
    self.match_summaries.select {|m| m.win == false }.count
  end

  def abbandons
    self.match_summaries.select {|m| m.leaver_status == 2}.count
  end

  def match_count
    self.match_summaries.count
  end

  def win_loss_ratio
    (self.wins.to_f / self.match_count.to_f) 
  end

  def average(field)
    self.match_summaries.avg(field)
  end

  def averages(fields)
    hash = Hash.new
    fields.each do |field|
      hash[field] = self.average(field)
    end
    return hash
  end

  class << self
    def from_omniauth(auth)
      # Convert 64-bit uid to 32-bit
      steam_64 = auth.uid.to_i
      steam_32 = SteamIdConverter.to_32(steam_64)
      user = where(uid: steam_32).first_or_create

      user.name = auth.info.name
      user.nickname = auth.info.nickname
      user.image = auth.info.image

      user.save!
      user
    end
  end
end
