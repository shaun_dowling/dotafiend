class FriendsController < ApplicationController
  def index
    @matches_count = current_user.match_summaries.count
    @user = User.where(uid: params[:uid]).first
    @friends = @user.friends
  end
end
