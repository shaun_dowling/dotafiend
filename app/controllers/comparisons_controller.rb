
class ComparisonsController < ApplicationController
  before_action :set_comparison, only: [:show, :edit, :update, :destroy]

  def index
    @user = User.where(uid: params[:uid]).first
    @comparisons = @user.comparisons
  end

  def show
  end

  def edit
  end

  def update
    if @comparison.update(comparison_hash)
      redirect_to comparison_path(current_user.uid, @comparison), notice: 'Sucessfully updated comparison'
    else
      render 'edit'
    end
  end

  def create
    @comparison = Comparison.new(comparison_hash)
    if @comparison.save!
      redirect_to comparisons_path(current_user.uid), notice: 'Sucessfully created comparison'
    else
      render 'new'
    end
  end

  def destroy
    if @comparison.destroy
      redirect_to comparisons_path, notice: 'Sucesfully deleted comparison'
    end
  end

  private
    def set_comparison
      @comparison = User.where(uid: params[:uid]).first.comparisons.find(params[:id])
    end

    def comparison_params
      params.require(:comparison).permit(:heroes, :players, :title)
    end

    def comparison_hash
      players = comparison_params["players"].split(",").uniq
      heroes = comparison_params["heroes"].split(",").map(&:to_i).uniq
      { title: comparison_params["title"], user_uid: current_user.uid, players: players, heroes: heroes }
    end
end
