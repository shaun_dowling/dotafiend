class AccountController < ApplicationController
  before_action :authenticate, :validate_user

  def show
    @user = @current_user
  end

  def update
    @user = current_user
    @user.update(user_params)
    flash[:success] = "Updated account settings"
    render 'show'
  end

  def request_match_history
    Dota.api.heroes.each do |hero|
      Resque.enqueue(HeroMatchesJob, current_user.uid, hero.id)
    end
    render json: { message: "success" }
  end

  private

    def user_params
      params.require(:user).permit(:email, :weekly_reports_preference, :feature_updates_preference)
    end
end
