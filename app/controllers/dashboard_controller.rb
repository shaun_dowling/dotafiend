class DashboardController < ApplicationController
  before_filter :authenticate

  def index
    @user = User.where(uid: params[:uid]).first
    @recent_matches = user.match_summaries.order_by(start_time: :desc).limit(6)
  end
end
