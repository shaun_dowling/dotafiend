
class SessionsController < ApplicationController
  skip_before_filter :verify_authenticity_token, only: :create

  def create
    begin
      @user = User.from_omniauth request.env['omniauth.auth']
    rescue
      flash[:error] = "Can't authorize you..."
      redirect_to root_path
    else
      session[:user_id] = @user.id

      # Add players friends
      Resque.enqueue(UserFriendsJob, @user.uid)

      flash[:success] = "Welcome, #{@user.nickname}!"
      redirect_to dashboard_path(@user.uid)
    end
  end

  def destroy
    if current_user
      session.delete(:user_id)
      flash[:success] = "Goodbye!"
    end
    redirect_to root_path
  end
end
