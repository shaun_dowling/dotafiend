class MatchesController < ApplicationController
  before_filter :authenticate

  def index
    @matches = @user.match_summaries.order_by(start_time: :desc).page(params[:page]).per(12)
  end
end
