class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  before_filter :current_user, :user

  private

    def authenticate
      redirect_to root_path unless @current_user
    end

    def current_user
      return nil unless session[:user_id]
      @current_user ||= User.find(session[:user_id])
    end

    def validate_user
      if @current_user.uid.to_s != params[:uid]
        redirect_to account_path(@current_user.uid)
      end
    end

    def user
      begin
        @user = User.where(uid: params[:uid]).first
      rescue
        @user = nil
      end
    end
end
