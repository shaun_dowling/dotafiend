# Installation

1. Download the source `git clone git@bitbucket.org:shaun_dowling/dotafiend.git`
2. change in to the project directory and install the dependencies `bundle install`
3. Ensure [RethinkDB](https://www.rethinkdb.com/) and [Redis](http://redis.io/) are installed and running

# Starting the server

This project uses foreman to manage different process types

First ensure RethinkDB and redis are running and accept connections then simply run `foreman start`
To run a specific process `foreman start PROCESS` consult the Procfile for available processes